# sodedit

A script to edit videos based on a sound detection algorithm. Currently only
supported on Linux, but could work on other operating systems.

## Getting Started

I haven't done a whole lot of testing and there's hardly any error handling in the script,
but you should be able to use it by just runing the script with a path to a video file
as the first parameter.
```bash
./sodedit.py "My Cool Video Thats too Long and Needs Cutting.mp4"
```
The output video will have the same name as the input, but with edit inserted between
the name and the file extension. For example video.mp4 would result in a video named video.edit.mp4.
There will also be a directory created with all the individual cuts which you can delete.


You may want to tweak the first few variables in the script to work best for
your video:
```python
# how loud the audio needs to be for that part of the clip to be included
# -50 being completely quiet and 0 the limit
loudness = -25
# the number of seconds to be added before and after each clip
clip_padding = 0.5
# the minumum length in seconds of the noise activity for it to be included
min_length = 0.1
```

### Prerequisites

You'll need ffmpeg and [ffmpeg-python](https://github.com/kkroening/ffmpeg-python) to be installed on your system

```
pip install ffmpeg-python
```
