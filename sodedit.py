#!/usr/bin/env python
import subprocess
import ffmpeg
import sys
import os
import re

# how loud the audio needs to be for that part of the clip to be included
# -50 being completely quiet and 0 the limit
loudness = -25
# the number of seconds to be added before and after each clip
clip_padding = 0.5
# the minumum length in seconds of the noise activity for it to be included
min_length = 0.1


input_file = sys.argv[1]
video_name = '.'.join((''.join(input_file.split("/")[-1])).split(".")[0:-1])
file_extension = input_file.split(".")[-1]
output_dir = 'edit-video_' + video_name

# ffmpeg-python is much faster than using subprocess which is why I'm using it
get_data = ffmpeg.input(input_file)
get_data = ffmpeg.filter_(get_data, 'astats', metadata=1, reset=1,)
get_data = ffmpeg.filter_(get_data, 'ametadata', 'print', key='lavfi.astats.Overall.RMS_level')
get_data = ffmpeg.output(get_data, '-', format='null')
data = ffmpeg.run(get_data, capture_stderr=True)
data = list(data)[1].decode('unicode_escape')

# this is how I would do it if I didn't have ffmpeg-python
#output = subprocess.run(['ffmpeg', '-i', input_file, '-af', 'astats=metadata=1:reset=1,ametadata=print:key=lavfi.astats.Overall.RMS_level',
                          #'-f', 'null', '-'], shell=False, capture_output=True)
#data = output.stderr.decode('unicode_escape')

# sample data:
#[Parsed_ametadata_1 @ 0x558f4518d540] frame:695  pts:711680  pts_time:16.1379
#[Parsed_ametadata_1 @ 0x558f4518d540] lavfi.astats.Overall.RMS_level=-65.846649

# extract the data we need
r = re.compile(r'(?:lavfi.astats.Overall.RMS_level=)(-[0-9.]*)')
levels = r.findall(data)
levels = [-50 if string == "-" else string for string in levels]
levels = [float(string) for string in levels]
r = re.compile(r'(?:pts_time\:)([0-9.]+)')
times = r.findall(data)
times = [float(string) for string in times]

# this list will contain all the times in the video where there's audio activity louder
# than the loudness set with the loudness variable.
use_times = []

for i in range(len(levels)):
    if levels[i] > loudness:
        use_times.append(times[i])

# this list will contain the start and end times we want to cut out of the original video.
use_ranges = []

x = 0
while x < len(use_times) - 1:
    start_time = use_times[x] - clip_padding
    if start_time < 0:
        start_time = 0

    # exclude clips that are shorter than minumum length
    # this will exclude short clicks for example
    if use_times[x+1] - use_times[x] > min_length:
        x += 1
        continue

    # as long we haven't reached the end of our list and the next audio activity (use_time)
    # is within clip_padding time
    while (x + 1 < len(use_times)) and (use_times[x+1] - use_times[x]) < clip_padding * 2:
        x += 1

    end_time = use_times[x] + clip_padding

    # end_time should not be greater than video length
    if end_time > times[-1]:
        end_time = use_times[x]
        # no clip should be shorter than 1 second.
        if start_time - use_times[x] < 1:
            break

    use_ranges.append({'start_time': start_time, 'end_time': end_time})
    x += 1


if os.path.exists(output_dir):
    print("A directory with name '%s' already exists" % (output_dir))
    sys.exit()
else:
    os.mkdir(output_dir)

video_list_file = open("%s/videolist.txt" % output_dir, "w")
for i in use_ranges:
    # sh = start hour, sm = start minute, ss = start second
    sm, ss = divmod(i['start_time'], 60)
    sh, sm = divmod(sm, 60)

    # dh = duration hours, dm = duration minutes, ds = duration seconds
    dm, ds = divmod(i['end_time'] - i['start_time'], 60)
    dh, dm = divmod(dm, 60)

    # I don't know how to use the -ss option with
    # ffmpeg-python so I'm calling ffmpeg with subprocess
    subprocess.run(['ffmpeg',
                    '-ss', '%d:%02d:%06.3f' % (sh, sm, ss),
                    '-i', input_file,
                    '-t', '%d:%02d:%06.3f' % (dh, dm, ds),
                    '-preset', 'faster',
                    '%s/%08.3f.%s' % (output_dir, i['start_time'], file_extension)])
    video_list_file.write("file '%08.3f.%s'\n" % (i['start_time'], file_extension))


video_list_file.close()

subprocess.run(['ffmpeg', '-f', 'concat', '-i', '%s/videolist.txt' % (output_dir), '-c', 'copy', '%s.edit.%s' % (video_name, file_extension)], check=True)
